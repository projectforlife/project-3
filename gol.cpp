/*
 * Contributors:
 Ka Shing Wai
 Yinuo Huang
 Zhuoying Cai
 * CSc103 Project 3: Game of Life
 * See readme.html for details.
 * Please list all references you made use of in order to complete the
 * assignment: your classmates, websites, etc.  Aside from the lecture notes
 * and the book, please list everything.  And remember- citing a source does
 * NOT mean it is okay to COPY THAT SOURCE.  What you submit here **MUST BE
 * YOUR OWN WORK**.
 * References:
 *
 *
 * Finally, please indicate approximately how many hours you spent on this:
 * #hours:
 */

#include <cstdio>
#include <stdlib.h> // for exit();
#include <getopt.h> // to parse long arguments.
#include <unistd.h> // sleep
#include <vector>
using std::vector;
#include <string>
using std::string;

static const char* usage =
"Usage: %s [OPTIONS]...\n"
"Text-based version of Conway's game of life.\n\n"
"   --seed,-s     FILE     read start state from FILE.\n"
"   --world,-w    FILE     store current world in FILE.\n"
"   --fast-fw,-f  NUM      evolve system for NUM generations and quit.\n"
"   --help,-h              show this message and exit.\n";

size_t max_gen = 0; /* if > 0, fast forward to this generation. */
string wfilename =  "/tmp/gol-world-current"; /* write state here */
FILE* fworld = 0; /* handle to file wfilename. */
string initfilename = "/tmp/gol-world-current"; /* read initial state from here. */

size_t nbrCount(size_t i, size_t j, const vector<vector<bool> >& g);
void update();
void initFromFile(const string& fname);
void mainLoop();
void dumpState(FILE* f, vector<vector<bool> > g, vector<bool> vec);

vector<vector<bool> >g;
vector<bool>vec;

char text[3] = ".O";

int main(int argc, char *argv[]) {
	// define long options
	static struct option long_opts[] = {
		{"seed",    required_argument, 0, 's'},
		{"world",   required_argument, 0, 'w'},
		{"fast-fw", required_argument, 0, 'f'},
		{"help",    no_argument,       0, 'h'},
		{0,0,0,0}
	};
	// process options:
	char c;
	int opt_index = 0;
	while ((c = getopt_long(argc, argv, "hs:w:f:", long_opts, &opt_index)) != -1) {
		switch (c) {
			case 'h':
				printf(usage,argv[0]);
				return 0;
			case 's':
				initfilename = optarg;
				break;
			case 'w':
				wfilename = optarg;
				break;
			case 'f':
				max_gen = atoi(optarg);
				break;
			case '?':
				printf(usage,argv[0]);
				return 1;
		}
	}
    initFromFile(initfilename);
	mainLoop();
	return 0;
}

void mainLoop()
{
	/* update, write, sleep */
	size_t i, j;
	int total;
	while (g.size() >= 0)
	{
	for(i = 0; i < g.size(); i++)
	{
		for(j = 0; j < vec.size(); j++)
		{
			total = nbrCount(i,j);
			dumpState(fopen(wfilename,"wb"),total,i,j);
			update();
		}
	}
	sleep(2);
	}
}

void initFromFile(const string& fname)//read old file
{
	FILE* f = fopen(fname.c_str(),"rb");
	char c;
	while(fread(&c,1,1,f)!= 0)
		{
	if(c == '\n')
		{
		g.push_back(vec);
		vec.clear();
		}
	else
		{
		if(c == '.')
			vec.push_back(false);
		else
			vec.push_back(true);
		}
		}
	fclose(f);
}

size_t nbrCount(size_t i,size_t j,const vector<vector<bool> >& g)
{
	size_t width = g[i].size();
	size_t length= g[j].size();
	int total = 0;

		if(g[i][(j-1+width)%width]== true)
			total++;
		if(g[i][(j+1+width)%width]== true)
			total++;
		if(g[(i-1+length)%length][j]== true)
			total++;
		if(g[(i+1+length)%length][j]== true)
			total++;
		if(g[(i-1+length)%length][(j-1+width)%width]== true)
			total++;
		if(g[(i+1+length)%length][(j-1+width)%width]== true)
			total++;
		if(g[(i-1+length)%length][(j+1+width)%width]== true)
			total++;
		if(g[(i+1+length)%length][(j+1+width)%width]== true)
			total++;

	return total;
}

void update()
{
	string fname = "/tmp/gol-world-current";
	FILE* f = fopen(fname.c_str(),"rb");
	char c;
	while(fread(&c,1,1,f)!= 0)
		{
	if(c == '\n')
		{
		g.push_back(vec);
		vec.clear();
		}
	else
		{
		if(c == '.')
			vec.push_back(false);
		else
			vec.push_back(true);
		}
		}
	fclose(f);
}

void dumpState(FILE* f, int total, size_t i, size_t j)
{
	switch (g[i][j]){
		case false: if (total == 3)
		{
			g[i][j]= 'O';
			break;
		}
		case true: if (total == 2 || total == 3)
		    {
				g[i][j] = 'O';
				break;
			}
			else
			{
				g[i][j] = '.';
				break;
				}
		}
		default:
			g[i][j] = '.';
		break;
}

